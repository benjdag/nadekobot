FROM microsoft/dotnet:2.1-sdk-stretch AS build

COPY . /nadekoBot

WORKDIR /nadekoBot/src/NadekoBot
RUN set -ex; \
    dotnet restore; \
    dotnet build -c Release; \
    dotnet publish -c Release -o /app

WORKDIR /app
RUN set -ex; \
    rm libopus.so libsodium.dll libsodium.so opus.dll; \
    find . -type f -exec chmod -x {} \;;

FROM mcr.microsoft.com/dotnet/core/runtime:2.1-stretch-slim AS runtime
WORKDIR /app
COPY --from=build /app /app
ARG DEBIAN_FRONTEND=noninteractive
RUN set -ex; \
    cat /etc/apt/sources.list; \
    echo "deb http://deb.debian.org/debian jessie-backports main" | tee /etc/apt/sources.list.d/debian-backports.list; \
    apt-get update &&  apt install ffmpeg -y; \
    apt-get install curl libunwind8 gettext -y; \
    apt-get install libopus0 opus-tools libopus-dev libsodium-dev redis-server -y; \
    apt-get install git curl wget sqlite -y; \
    apt-get install tmux python python3.5 -y; \
    wget https://yt-dl.org/downloads/latest/youtube-dl -O /usr/local/bin/youtube-dl; \
    chmod a+rx /usr/local/bin/youtube-dl; \
    adduser --disabled-password --home /home/container container; \
    chown container /app; \
    chmod u+w /app; \
    mv /app/data /app/data-default; \
    install -d -o container -g container -m 755 /app/data;

# workaround for the runtime to find the native libs loaded through DllImport
RUN set -ex; \
    ln -s /usr/lib/libopus.so.0 /app/libopus.so; \
    ln -s /usr/lib/libsodium.so.23 /app/libsodium.so

USER container
ENV         HOME=/home/container USER=container
WORKDIR     /home/container

COPY ./entrypoint.sh /entrypoint.sh
CMD ["/bin/bash","/entrypoint.sh"]
